import telebot
from telebot import types
import requests
import json
from pymongo import MongoClient
from DataBase import DB

client = MongoClient()
db = client.first_database
collection = db.user_collection

token = "402598316:AAEjBxX_ohTfJqcDdPrACu5RLVpscZZ08hA"
bot = telebot.TeleBot(token)


class BotСs():
    def __init__(self, message):

        self.message = message

    def init_kb_slider(self, bt1, bt2):
        keyboard_sl = types.InlineKeyboardMarkup(row_width=1)
        button1 = types.InlineKeyboardButton(text='<-', callback_data="{}".format(bt1-3))
        button2 = types.InlineKeyboardButton(text='->', callback_data="{}".format(bt2))
        button3 = types.InlineKeyboardButton(text='Назад к фильтру', callback_data='back')
        btns = []
        if bt1 > 0:
            btns.append(button1)
        if bt2 < len(posts):
            # btns.append(button3)
            btns.append(button2)
        btns.append(button3)
        keyboard_sl.add(*btns)
        return keyboard_sl

    def init_spis_fltr_tenders(self):
        keyboard = types.InlineKeyboardMarkup(row_width=1)
        button1 = types.InlineKeyboardButton(text="Требует внимания", callback_data="aaaaaa")
        button2 = types.InlineKeyboardButton(text="Ожидает ответа ", callback_data="bbbbbb")
        button3 = types.InlineKeyboardButton(text="В работе", callback_data="cccccc")
        button4 = types.InlineKeyboardButton(text="Завершенные", callback_data="dddddd")
        button5 = types.InlineKeyboardButton(text="Скрытые", callback_data="eeeeee")
        button6 = types.InlineKeyboardButton(text="Все", callback_data="ffffff")
        keyboard.add(button1, button2, button3, button4, button5, button6)
        # bot.send_message(self.message.chat.id,"Фильтр тендеров:",reply_markup=keyboard) не отдавать клаву,чтобы задавать меняющейся путь к объекту динамично
        return keyboard

    def read_and_format(self):
        # x=list(collection.find())
        work_with_base = DB  # callback_data+2 [:]   Не вижу возможности реализации!!!
        global posts  # UГлобальная переменная для передачи состояния слайдера
        posts = []
        for x in work_with_base:
            post = [
                "*Марка* : {mark} *Регистрационный №:*{reg_num}",
                "*{head_post}* _{body_post}_ ",
                "*Дата начала*: _{data_start}_" "*Дата окончания*:{data_end}",
                "*{status_order}*",
                "....."]
            post = "\n".join(post)
            z = post.format(**x)
            posts.append(z)
        bot.edit_message_reply_markup(chat_id=self.message.message.chat.id, message_id=self.message.message.message_id)
        bot.send_message(self.message.message.chat.id, text="\n".join(posts[0:3]),
                         parse_mode="Markdown", reply_markup=self.init_kb_slider(0,+3))


@bot.message_handler(commands=["start"]) # Если телефон в базе данны есть, отдать тендеры, если нет, запросить контакты для авторизации
def keybord_down(message):
    print(message)

    init_person = []
    list_col_find = list(collection.find())
    for x in list_col_find:
        y = x.get("user_id")
        init_person.append(y)
    print(init_person, message.from_user.id)

    if int(message.from_user.id) in init_person:
        print("itsWORK")
        keybord = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
        tender_button = types.KeyboardButton("/tenders")
        keybord.add(tender_button)
        bot.send_message(message.chat.id, "Добро пожаловать в личный кабинет владельца СТО", reply_markup=keybord)

    else:
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
        batton = types.KeyboardButton("Логин по номеру телефона", request_contact=True)
        markup.add(batton)
        bot.send_message(message.chat.id, "Welcom", reply_markup=markup)
        bot.register_next_step_handler(message, next_step)


def next_step(message):
    print(message)
    # Если в БД ремонтисыты есть номер телефона из поля "contact" зарегестрировать,отдать клаву. Если нет, отдать текст отказа
    contact = eval(str(message.contact))
    collection.insert(contact)

    print("itsWORK")
    keybord = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    tender_button = types.KeyboardButton("/tenders")
    keybord.add(tender_button)
    bot.send_message(message.chat.id, "Добро пожаловать в личный кабинет владельца СТО", reply_markup=keybord)


@bot.message_handler(commands=["tenders"])
def kb_ferst_step(message):
    # print(message)
    brain = BotСs(message)
    brain.init_spis_fltr_tenders()
    bot.send_message(message.chat.id, "Фильтр тендров:", reply_markup=brain.init_spis_fltr_tenders())


@bot.callback_query_handler(func=lambda message: len(message.data) == 6)  # по каким запросам из базы я получу нужный фильтр ? ЛОВИТ 6и СИМВОЛЬНЫЕ СТРОКИ
def printer(message):  # по if сделает запрос к бд по номеру телефона уложив в список, отдаст в read_and_format
    print(message, "Ferst HANDLER")
    brain = BotСs(message)
    brain.read_and_format()


@bot.callback_query_handler(func=lambda message: message.data == "back")
def slider_handler(message):
    brain = BotСs(message)
    brain.init_spis_fltr_tenders()

    bot.edit_message_text(
        chat_id=message.message.chat.id,
        message_id=message.message.message_id,
        text="Фильтр тендеров:",
        parse_mode='Markdown',
        reply_markup=brain.init_spis_fltr_tenders()
    )


@bot.callback_query_handler(func=lambda message: message.data)
def slider_handler(message):
    print(message, "на месте")
    brain = BotСs(message)
    bot.edit_message_text(
        chat_id=message.message.chat.id,
        message_id=message.message.message_id,
        text="\n".join(posts[int(message.data):int(message.data)+3]),  # text=BOOK[int(c.data[3:]):int(c.data[3:]) + 700],
        parse_mode='Markdown',
        reply_markup=brain.init_kb_slider(int(message.data), int(message.data)+3))
    print(message)


def messenger():
    init_person = []
    list_col_find = list(collection.find())
    for x in list_col_find:
        y = x.get("user_id")
        init_person.append(y)
    print(init_person)


bot.polling(none_stop=True)
